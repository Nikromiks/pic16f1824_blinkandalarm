#if defined(__XC)
#include <xc.h>         /* XC8 General Include File */
#elif defined(HI_TECH_C)
#include <htc.h>        /* HiTech General Include File */
#endif
#include "appAlarm.h"
#include "appBlink.h"
#include "time.h"
#include "main.h"

char trig_alarm = 0;
unsigned long int timeAlarm = 0;

void initAlarm(void) {
    if (IN_ALARM == 0) {
        trig_alarm = 1;
        if (eeprom_countAlarm == 1)
            timeAlarm = 1200000;
        if (eeprom_countAlarm == 2)
            timeAlarm = 2400000;
        if (eeprom_countAlarm == 3)
            timeAlarm = 3600000;
        if (eeprom_countAlarm == 4)
            timeAlarm = 5400000;
        if (eeprom_countAlarm == 5)
            timeAlarm = 7200000;
    } else
        trig_alarm = 0;
    OUT_ALARM_TIMER(1);
}

void appAlarm(void) {
    if ((trig_alarm == 1) && timeCount > timeAlarm) {
        OUT_ALARM_TIMER(0);
        trig_alarm = 0;
    }
}
