
/******************************************************************************/
/*Files to Include                                                            */
/******************************************************************************/

#if defined(__XC)
    #include <xc.h>         /* XC8 General Include File */
#elif defined(HI_TECH_C)
    #include <htc.h>        /* HiTech General Include File */
#endif

volatile unsigned long int timeCount = 0;

/**
 * Return Time in ms
 * @return time in ms
 */
unsigned long int getTime(){
    return timeCount;
}

void delayMs(int ms){
    unsigned long int timePause = getTime() + ms;
    while(getTime() < timePause);
}