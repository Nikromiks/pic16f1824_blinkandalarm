/******************************************************************************/
/* Files to Include                                                           */
/******************************************************************************/
#if defined(__XC)
#include <xc.h>         /* XC8 General Include File */
#elif defined(HI_TECH_C)
#include <htc.h>        /* HiTech General Include File */
#endif

#include "appBlink.h"
#include "appThanksProg.h"
#include "main.h"
#include "time.h"
#include "eeprom.h"

#define COUNT_TRY 2

/******************************************************************************/
/* User Functions                                                             */
/******************************************************************************/
static unsigned long int timeWait = 0;
static unsigned long int timeWaitLeft = 0;
static unsigned long int timeWaitRight = 0;
static unsigned long int timeWaitPWM = 0;
static unsigned long int timeWaitStartProg = 0;

static unsigned long int timeNotWorkLeft = 0;
static unsigned long int timeNotWorkRight = 0;
static unsigned char flagStrob = 0;

volatile stateEnum stateBlink = WAIT_START;

static unsigned long int timeSumPulse = 0;
static unsigned char countPulse = 0;

static unsigned char blinkerPrev;

static volatile unsigned char sensor = 0;

static unsigned long int left;
static unsigned long int right;

static unsigned char checkTrig = 0;

void lowOut() {
    OUT_RIGHT_LOW_BLINKER(0);
    OUT_LEFT_LOW_BLINKER(0);
    OUT_RIGHT_HIGH_BLINKER(0);
    OUT_LEFT_HIGH_BLINKER(0);
    OUT_LEFT_LOW_BLINKER(0);
}

void updateTimes() {
    timeWaitRight = timeCount;
    timeWaitLeft = timeCount;
}

void setBlinker(blinkEnum blink) {
    flagStrob = 1;
    leftRigth = blink;
    timeWait = timeCount;
    countPulse = 1;
}

void startProgramming() {
    timeWaitStartProg = timeCount + TIME_WAIT_START_PROG;
    if (type == HIGH)
        stateBlink = WAIT_PROG_HIGH;
    else
        stateBlink = WAIT_PROG_LOW;
}

int waitStartProg() {
    return (timeCount > timeWaitStartProg);
}

stateEnum getStateBlink() {
    return stateBlink;
}

void setStateBlink(stateEnum newState) {
    stateBlink = newState;
}

void pulseStrob(int pause) {
    if (type == HIGH) {
        OUT_RIGHT_HIGH_BLINKER(1);
        OUT_LEFT_HIGH_BLINKER(1);
    } else {
        OUT_RIGHT_LOW_BLINKER(1);
        OUT_LEFT_LOW_BLINKER(1);
    }
    delayMs(pause);
    lowOut();
    delayMs(pause);
}

void appBlinkWork() {
    if (getStateProgThanks() != WORK) {
        if (type == HIGH)
            stateBlink = WORK_HIGH;
        if (type == LOW)
            stateBlink = WORK_LOW;
        return;
    }


    if (stateBlink == WORK_HIGH_BLINK || stateBlink == WORK_HIGH || stateBlink == WAIT_PROG_HIGH) {
        if (IN_LEFT_BLINKER != 0) {
            timeWaitLeft = timeCount;
        }
        if (IN_RIGHT_BLINKER != 0) {
            timeWaitRight = timeCount;
        }
        left = timeCount - timeWaitLeft;
        right = timeCount - timeWaitRight;
    }



    if (stateBlink == WAIT_START) {
        if (timeCount < TIME_WAIT_START) {
            if (IN_LEFT_BLINKER != 0 || IN_RIGHT_BLINKER != 0)
                timeWaitPWM = timeCount;
        } else {
            stateBlink = START;
        }
        return;
    }
    if (stateBlink == START) {
        if ((timeCount - timeWaitPWM) < TIME_WAIT_PWM ||
                IN_NULL == 0) {
            type = HIGH;
            stateBlink = WORK_HIGH;
        } else {
            type = LOW;
            stateBlink = WORK_LOW;
        }
        return;
    }

    if (stateBlink == WAIT_PROG_HIGH) {
        if (waitStartProg() != 1) {
            if (right > TIME_WAIT_PWM) {
                stateBlink = PROG_HIGH;
                setBlinker(RIGTH);
                return;
            }
            if (left > TIME_WAIT_PWM) {
                stateBlink = PROG_HIGH;
                setBlinker(LEFT);
                return;
            }
        } else {
            stateBlink = WORK_HIGH;
            pulseStrob(TIME_PULSE_OUT_PROG);
        }
        return;
    }
    if (stateBlink == WAIT_PROG_LOW) {
        if (waitStartProg() != 1) {
            if (IN_RIGHT_BLINKER == 1
                    || IN_LEFT_BLINKER == 1) {
                if (IN_RIGHT_BLINKER == 1)
                    setBlinker(RIGTH);
                else
                    setBlinker(LEFT);
                eeprom_timePeriod = timeCount;
                timeWait = timeCount;
                countPulse = 1;
                timeSumPulse = 0;
                flagStrob = 0;
                eeprom_timePulse = 2000;
                stateBlink = PROG_LOW;
            }
        } else {
            stateBlink = WORK_LOW;
            pulseStrob(TIME_PULSE_OUT_PROG);
        }
        return;
    }
    if (stateBlink == PROG_HIGH) {
        if (leftRigth == RIGTH && IN_RIGHT_BLINKER != 0) {
            if (IN_RIGHT_BLINKER != 0) {
                eeprom_timePeriod = timeCount - timeWaitRight;
                saveToEEPROM();
                pulseStrob(TIME_PULSE_OUT_PROG);
                pulseStrob(TIME_PULSE_OUT_PROG);
                stateBlink = WORK_HIGH;
            }
            return;
        }
        if (leftRigth == LEFT && IN_LEFT_BLINKER != 0) {
            if (IN_LEFT_BLINKER != 0) {
                eeprom_timePeriod = timeCount - timeWaitLeft;
                saveToEEPROM();
                pulseStrob(TIME_PULSE_OUT_PROG);
                pulseStrob(TIME_PULSE_OUT_PROG);
                stateBlink = WORK_HIGH;
            }
        }
        return;
    }
 if (stateBlink == PROG_LOW) {
        if ((timeCount - timeWait) > (eeprom_timePulse + eeprom_timePulse / 10)) {
            eeprom_timePeriod = (timeSumPulse + countPulse * 150);
            eeprom_timePulse = eeprom_timePeriod / countPulse;
            eeprom_countPulse = (char) (countPulse >> 1);
            saveToEEPROM();
            pulseStrob(TIME_PULSE_OUT_PROG);
            pulseStrob(TIME_PULSE_OUT_PROG);
            stateBlink = WORK_LOW;
            return;
        }
        if (flagStrob == 0)
            if ((IN_RIGHT_BLINKER == 0 && leftRigth == RIGTH)
                    || (IN_LEFT_BLINKER == 0 && leftRigth == LEFT)) {
				if(checkTrig == 0) {
					checkTrig = 1;
					delayMs(5);
					return;
				}
				checkTrig = 0;
                countPulse++;
                timeSumPulse = timeSumPulse + (timeCount - timeWait);
                timeWait = timeCount;
                flagStrob = 1;
                return;
            }
        if (flagStrob == 1)
            if ((IN_RIGHT_BLINKER == 1 && leftRigth == RIGTH)
                    || (IN_LEFT_BLINKER == 1 && leftRigth == LEFT)) {
				if(checkTrig == 0) {
					checkTrig = 1;
					delayMs(5);
					return;
				}
				checkTrig = 0;				
                countPulse++;
                timeSumPulse = timeSumPulse + (timeCount - timeWait);
                timeWait = timeCount;
                flagStrob = 0;
                delayMs(20);
                return;
            }
		checkTrig = 0;
        return;
    }
    if (stateBlink == WORK_HIGH) {
        if (right > TIME_WAIT_PWM && left < TIME_WAIT_PWM) {
            if (flagStrob > COUNT_TRY) {
                setBlinker(RIGTH);
                timeWaitRight = timeCount;
                timeWaitLeft = timeCount;
                stateBlink = WORK_HIGH_BLINK;
                flagStrob = 0;
                OUT_RIGHT_HIGH_BLINKER(1);
                return;
            }
            flagStrob++;
            return;
        }
        if (left > TIME_WAIT_PWM && right < TIME_WAIT_PWM) {
            if (flagStrob > COUNT_TRY) {
                setBlinker(LEFT);
                timeWaitRight = timeCount;
                timeWaitLeft = timeCount;
                stateBlink = WORK_HIGH_BLINK;
                flagStrob = 0;
                OUT_LEFT_HIGH_BLINKER(1);
                return;
            }
            flagStrob++;
            return;
        }
        flagStrob = 0;
        return;
    }
    if (stateBlink == WORK_HIGH_BLINK) {
        if ((timeCount - timeWait) > eeprom_timePeriod) {
            lowOut();
            updateTimes();
            stateBlink = WORK_HIGH;
            return;
        }
        if (leftRigth == RIGTH) {
            if (left > TIME_WAIT_PWM) {
                if (flagStrob > COUNT_TRY) {
                    setBlinker(LEFT);
                    lowOut();
                    updateTimes();
                    flagStrob = 0;
                    OUT_LEFT_HIGH_BLINKER(1);
                }
                flagStrob++;
                return;
            }
            return;
        }
        if (leftRigth == LEFT) {
            if (right > TIME_WAIT_PWM) {
                if (flagStrob > COUNT_TRY) {
                    setBlinker(RIGTH);
                    lowOut();
                    updateTimes();
                    flagStrob = 0;
                    OUT_RIGHT_HIGH_BLINKER(1);
                }
                flagStrob++;
                return;
            }
            return;
        }
        flagStrob = 0;
        return;
    }
    if (stateBlink == TRIG_LEFT) {
        if (IN_RIGHT_BLINKER == 1 && IN_LEFT_BLINKER == 0) {
            timeWaitPWM = timeCount;
            stateBlink = TRIG_RIGHT;
            return;
        }
        if (IN_RIGHT_BLINKER == 1 && IN_LEFT_BLINKER == 1) {
            stateBlink = WORK_LOW;
            return;
        }
        if (IN_RIGHT_BLINKER == 0 && IN_LEFT_BLINKER == 0) {
            if ((timeCount - timeWaitPWM) < TIME_WAIT_LOW_LEVEL) {
                stateBlink = WORK_LOW;
                return;
            }
            if ((timeCount - timeWaitPWM) > TIME_WAIT_LOW_LEVEL &&
                    (timeCount - timeWaitPWM) < eeprom_timePulse * 0.85 &&
                    (timeCount - timeNotWorkLeft) > eeprom_timePulse * 3) {
                setBlinker(LEFT);
                blinkerPrev = 0;
                OUT_LEFT_LOW_BLINKER(1);
                stateBlink = WORK_BLINK_LOW;
                return;
            }
            if ((timeCount - timeWaitPWM) > eeprom_timePulse) {
                stateBlink = WORK_LOW;
                timeNotWorkLeft = timeCount;
                return;
            }
        }
        return;
    }
    if (stateBlink == TRIG_RIGHT) {
        if (IN_RIGHT_BLINKER == 0 && IN_LEFT_BLINKER == 1) {
            timeWaitPWM = timeCount;
            stateBlink = TRIG_LEFT;
            return;
        }
        if (IN_RIGHT_BLINKER == 1 && IN_LEFT_BLINKER == 1) {
            stateBlink = WORK_LOW;
            return;
        }
        if (IN_RIGHT_BLINKER == 0 && IN_LEFT_BLINKER == 0) {
            if ((timeCount - timeWaitPWM) < TIME_WAIT_LOW_LEVEL) {
                stateBlink = WORK_LOW;
                return;
            }
            if ((timeCount - timeWaitPWM) > TIME_WAIT_LOW_LEVEL &&
                    (timeCount - timeWaitPWM) < eeprom_timePulse * 0.85 &&
                    (timeCount - timeNotWorkRight) > eeprom_timePulse * 3) {
                setBlinker(RIGTH);
                stateBlink = WORK_BLINK_LOW;
                blinkerPrev = 0;
                OUT_RIGHT_LOW_BLINKER(1);
                return;
            }
            if ((timeCount - timeWaitPWM) > eeprom_timePulse) {
                stateBlink = WORK_LOW;
                timeNotWorkRight = timeCount;
                return;
            }
        }
        return;
    }
    if (stateBlink == WORK_LOW) {
        if (IN_LEFT_BLINKER == 1 && IN_RIGHT_BLINKER == 1) {
            return;
        }
        if (IN_LEFT_BLINKER == 1 && IN_RIGHT_BLINKER == 0) {
            timeWaitPWM = timeCount;
            lowOut();
            stateBlink = TRIG_LEFT;
            return;
        }
        if (IN_RIGHT_BLINKER == 1 && IN_LEFT_BLINKER == 0) {
            timeWaitPWM = timeCount;
            lowOut();
            stateBlink = TRIG_RIGHT;
            return;
        }
        return;
    }
    if (stateBlink == WORK_BLINK_LOW) {
        if (leftRigth == RIGTH) {
            if (flagStrob == 1) {
                if ((timeCount - timeWaitPWM) > eeprom_timePulse) {
                    flagStrob = 0;
                    lowOut();
                    timeWaitPWM = timeCount;
                    if (countPulse >= eeprom_countPulse) {

                        stateBlink = WORK_LOW;
                        return;
                    }
                }
            } else {
                if ((timeCount - timeWaitPWM) > eeprom_timePulse) {
                    flagStrob = 1;
                    countPulse++;
                    OUT_RIGHT_LOW_BLINKER(1);
                    timeWaitPWM = timeCount;
                }
            }
            if (IN_LEFT_BLINKER == 1 && IN_RIGHT_BLINKER == 0) {
                timeWaitPWM = timeCount;
                lowOut();
                stateBlink = TRIG_LEFT;
                return;
            }
            if (IN_LEFT_BLINKER == 0) {
                blinkerPrev == 0;
            }
            if (IN_RIGHT_BLINKER == 1 && IN_LEFT_BLINKER == 1) {
                if (blinkerPrev == 0) {
                    timeWaitRight = timeCount;
                    blinkerPrev = 1;
                } else {
                    if ((timeCount - timeWaitRight) > TIME_WAIT_BLINK) {
                        blinkerPrev = 0;
                        OUT_RIGHT_LOW_BLINKER(0);
                        delayMs(10);
                        if (IN_RIGHT_BLINKER == 1 && IN_LEFT_BLINKER == 1) {
                            stateBlink = WORK_LOW;
                            return;
                        }
                        if (IN_RIGHT_BLINKER == 0 && IN_LEFT_BLINKER == 1) {
                            timeWaitPWM = timeCount;
                            lowOut();
                            stateBlink = TRIG_LEFT;
                            return;
                        }
                        OUT_RIGHT_LOW_BLINKER(1);
                    }
                }

            }
        }
        if (leftRigth == LEFT) {
            if (flagStrob == 1) {
                if ((timeCount - timeWaitPWM) > eeprom_timePulse) {
                    flagStrob = 0;
                    lowOut();
                    timeWaitPWM = timeCount;
                    if (countPulse >= eeprom_countPulse) {
                        stateBlink = WORK_LOW;
                        return;
                    }
                }
            } else {
                if ((timeCount - timeWaitPWM) > eeprom_timePulse) {
                    flagStrob = 1;
                    countPulse++;
                    OUT_LEFT_LOW_BLINKER(1);
                    timeWaitPWM = timeCount;
                }
            }

            if (IN_RIGHT_BLINKER == 0) {
                blinkerPrev == 0;
            }
            if (IN_RIGHT_BLINKER == 1 && IN_LEFT_BLINKER == 1) {
                if (blinkerPrev == 0) {
                    timeWaitLeft = timeCount;
                    blinkerPrev = 1;
                } else {
                    if ((timeCount - timeWaitLeft) > TIME_WAIT_BLINK) {
                        blinkerPrev = 0;
                        OUT_LEFT_LOW_BLINKER(0);
                        delayMs(10);
                        if (IN_RIGHT_BLINKER == 1 && IN_LEFT_BLINKER == 1) {
                            stateBlink = WORK_LOW;
                            return;
                        }
                        if (IN_RIGHT_BLINKER == 1 && IN_LEFT_BLINKER == 0) {
                            timeWaitPWM = timeCount;
                            lowOut();
                            stateBlink = TRIG_RIGHT;
                            return;
                        }
                        OUT_LEFT_LOW_BLINKER(1);
                    }
                }

            }
            if (IN_RIGHT_BLINKER == 1 && IN_LEFT_BLINKER == 0) {
                timeWaitPWM = timeCount;
                lowOut();
                stateBlink = TRIG_RIGHT;
                return;
            }
        }

        return;
    }
}



