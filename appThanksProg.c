#if defined(__XC)
#include <xc.h>         /* XC8 General Include File */
#elif defined(HI_TECH_C)
#include <htc.h>        /* HiTech General Include File */
#endif

#include "appThanksProg.h"
#include "time.h"
#include "eeprom.h"
#include "main.h"
#include "appBlink.h"

static volatile stateProgThanksEnum stateProg = WORK;
static unsigned long int timeWaitThanks = 0;
static unsigned char flagStrobThanks;
static unsigned char countPulseThanks = 0;
static unsigned char countPress = 0;
static unsigned char press = 0;
static unsigned long int timeWaitProgTimer = 0;
static unsigned long int timeWaitTrig = 0;

stateProgThanksEnum getStateProgThanks() {
    return stateProg;
}

void appThanksProg() {
    if (getStateBlink() != WORK_LOW && getStateBlink() != WORK_HIGH) {
        stateProg = WORK;
        return;
    }
    if (stateProg == WORK) {
        if (IN_THANKS_PROG == 0) {
            timeWaitThanks = timeCount;
            stateProg = WAIT_THANKS;
        }
        return;
    }
    if (stateProg == WAIT_THANKS) {
        if (IN_THANKS_PROG == 0) {
            if ((timeCount - timeWaitThanks) > TIME_WAIT_LOW_LEVEL) {
                stateProg = WAIT_PROG;
            }
        } else {
            stateProg = WORK;
        }
        return;
    }
    if (stateProg == WAIT_PROG) {
        if (IN_THANKS_PROG == 0) {
            if ((timeCount < TIME_PROG_LIMIT)) {
                if ((type == LOW) && (timeCount - timeWaitThanks) > (TIME_WAIT_START_PROG + TIME_WAIT_START_PROG / 5)) {
                    pulseStrob(TIME_PULSE_IN_PROG);
                    stateProg = START_PROG;
                    return;
                }
                if ((type == HIGH) && (timeCount - timeWaitThanks) > TIME_WAIT_START_PROG) {
                    pulseStrob(TIME_PULSE_IN_PROG);
                    stateProg = START_PROG;
                    return;
                }
            }
        } else {
            stateProg = BLINK_THANKS;
            countPulseThanks = 0;
            flagStrobThanks = 0;
            timeWaitThanks = timeCount;
        }
        return;
    }
    if (stateProg == BLINK_THANKS) {
        if (type == LOW) {
            if (flagStrobThanks == 1) {
                if ((timeCount - timeWaitThanks) > TIME_THANKS_PULSE) {
                    flagStrobThanks = 0;
                    lowOut();
                    timeWaitThanks = timeCount;
                    if (countPulseThanks >= COUNT_THANKS_PULSE) {
                        setStateBlink(WORK_LOW);
                        stateProg = WORK;
                        return;
                    }
                }
            } else {
                if ((timeCount - timeWaitThanks) > TIME_THANKS_PULSE) {
                    countPulseThanks++;
                    flagStrobThanks = 1;
                    OUT_LEFT_LOW_BLINKER(1);
                    OUT_RIGHT_LOW_BLINKER(1);
                    timeWaitThanks = timeCount;
                    return;
                }
            }
        } else {
            if ((timeCount - timeWaitThanks) < TIME_THANKS) {
                OUT_RIGHT_HIGH_BLINKER(1);
                OUT_LEFT_HIGH_BLINKER(1);
            } else {
                lowOut();
                setStateBlink(WORK_HIGH);
                stateProg = WORK;
            }
            return;
        }
        return;
    }

    if (stateProg == START_PROG) {
        if (IN_THANKS_PROG == 0) {
            if ((timeCount - timeWaitThanks) > TIME_WAIT_START_PROG * 2) {
                stateProg = START_PROG_TIMER;
                flagStrobThanks = 0;
                countPress = 0;
                pulseStrob(TIME_PULSE_IN_PROG);
                pulseStrob(TIME_PULSE_IN_PROG);
                timeWaitProgTimer = timeCount;
                return;
            }
        } else {
            startProgramming();
            stateProg = WORK;
            return;
        }
        return;
    }

    if (stateProg == START_PROG_TIMER) {
        if (countPress != 0 && (timeCount - timeWaitTrig) > TIME_PULSE) {
            press = countPress;
            while (countPress-- > 0) {
                pulseStrob(300);
            }
            countPress = 0;
            timeWaitProgTimer = timeCount;
        }
        if (IN_THANKS_PROG == 0) {
            if (flagStrobThanks == 0) {
                flagStrobThanks = 1;
            }
        } else {
            if (flagStrobThanks == 1) {
                if ((timeCount - timeWaitThanks) > TIME_WAIT_LOW_LEVEL) {
                    countPress++;
                    if (countPress % 6 == 0) countPress = 1;
                    timeWaitTrig = timeCount;
                    flagStrobThanks = 0;
                }
            }
        }
        if (countPress == 0 && (timeCount - timeWaitProgTimer) > TIME_WAIT_START_PROG) {
            eeprom_countAlarm = press;
            saveToEEPROM();
            stateProg = WORK;
            pulseStrob(500);
            while (press-- > 0) {
                pulseStrob(200);
            }
            return;
        }
        return;
    }
}