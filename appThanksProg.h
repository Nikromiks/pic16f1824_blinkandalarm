/* 
 * File:   appThanksProg.h
 * Author: User
 *
 * Created on 19 ?????? 2013 ?., 21:59
 */

#ifndef APPTHANKSPROG_H
#define	APPTHANKSPROG_H

#ifdef	__cplusplus
extern "C" {
#endif

/*
 * State prog or thanks app
 */
typedef enum{
    WORK, WAIT_THANKS, WAIT_PROG, BLINK_THANKS, START_PROG,
            START_PROG_TIMER
}stateProgThanksEnum;

void appThanksProg();
stateProgThanksEnum getStateProgThanks();

#ifdef	__cplusplus
}
#endif

#endif	/* APPTHANKSPROG_H */

